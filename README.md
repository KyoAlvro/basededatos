# BaseDeDAtos

-- crear usuario --
CREATE USER 'KyoSENATI'@'localhost' IDENTIFIED BY 'KyoSENATI';
-- CONCEDER PRIVILEGIOS --
GRANT SELECT ON EmpresaDB.Departamento TO 'KyoSENATI'@'localhost';
GRANT INSERT ON EmpresaDB.Empleado TO 'KyoSENATI'@'localhost';
GRANT UPDATE ON EmpresaDB.Proyecto TO 'KyoSENATI'@'localhost';

-- REVOCAR PRIVILEGIOS --
REVOKE SELECT ON EmpresaDB.Departamento FROM 'KyoSENATI'@'localhost'
REVOKE INSERT ON EmpresaDB.Empleado FROM 'KyoSENATI'@'localhost';
REVOKE UPDATE ON EmpresaDB.Proyecto FROM 'KyoSENATI'@'localhost';

-- COMMIT / ROLLBACK -- 

START TRANSACTION;
INSERT INTO empresadb.Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');
COMMIT;
ROLLBACK;

-- SAVE POINT -- 
START TRANSACTION;
SELECT * FROM empresadb.empleado
INSERT INTO empresadb.Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;
INSERT INTO empresadb.Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 2, '2023-01-01', 3500, NULL, 10);
ROLLBACK TO Savepoint1;
COMMIT;

-- SET TRANSACTION -- 
START TRANSACTION;
SELECT * FROM empresadb.empleado
INSERT INTO empresadb.Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;
INSERT INTO empresadb.Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 2, '2023-01-01', 3500, NULL, 10);
ROLLBACK TO Savepoint1;
COMMIT;

-- Creación de la tabla con tipos de datos optimizados y restricciones -- 

CREATE DATABASE EmpleadoDB;
USE EmpleadoDB;

CREATE TABLE Departamento (
    IDDepartamento INT AUTO_INCREMENT PRIMARY KEY,
    NombreDepartamento VARCHAR(50) NOT NULL UNIQUE,
    Ubicacion VARCHAR(50) DEFAULT 'UNKNOWN'
);

CREATE TABLE Empleado (
    IDEmpleado INT AUTO_INCREMENT PRIMARY KEY,
    NombreEmpleado VARCHAR(50) NOT NULL,
    Puesto VARCHAR(50),
    IDJefe INT,
    FechaContratacion DATE DEFAULT CURRENT_DATE,
    Salario DECIMAL(10,2) CHECK (Salario >= 0),
    Comision DECIMAL(10,2) CHECK (Comision >= 0),
    IDDepartamento INT,
    FOREIGN KEY (IDDepartamento) REFERENCES Departamento(IDDepartamento),
    FOREIGN KEY (IDJefe) REFERENCES Empleado(IDEmpleado)

-- cursores -- 

DELIMITER //
CREATE PROCEDURE ListarEmpleados()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE ID INT;
    DECLARE Nombre VARCHAR(50);
    DECLARE EmpleadoCursor CURSOR FOR SELECT IDEmpleado, NombreEmpleado From empleado;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done =1;
    open EmpleadoCursor;
    REPEAT
        FETCH EmpleadoCursor INTO ID, Nombre;
        IF NOT done THEN
            SELECT ID, Nombre;

        end if;
    until done end REPEAT;
    close EmpleadoCursor;
end//
DELIMITER ;
CALL ListarEmpleados();

SELECT * FROM empresadb.empleado;

-- ALIAS --

SELECT E.NombreEmpleado AS Nombre, D.NombreDepartamento AS Departamento
FROM Empleado E
JOIN Departamento D ON E.IDDepartamento = D.IDDepartamento;
-- PARAMETRIZACION --
PREPARE stmt FROM 'SELECT * FROM Empleado WHERE Salario > ?';
SET @SalarioMin = 2500.00;
EXECUTE stmt USING @SalarioMin;
DEALLOCATE PREPARE stmt;
-- EXISTS --
SELECT NombreEmpleado
FROM Empleado E
WHERE EXISTS (
    SELECT 1
    FROM Empleado EP
    WHERE EP.IDEmpleado = E.IDEmpleado
);

-- DISTINCT --

SELECT DISTINCT NombreDepartamento
FROM Departamento;

-- * --

SELECT * FROM empleado;

-- VERIFICAR SI UN REGISTRO EXISTE --

IF (SELECT EXISTS (SELECT 1 FROM Empleado WHERE NombreEmpleado = 'SMITH')) THEN
    SELECT 'Empleado existe';
ELSE
    SELECT 'Empleado no existe';
END IF;

-- ORDER BY --

SELECT * FROM Empleado ORDER BY NombreEmpleado ASC;

